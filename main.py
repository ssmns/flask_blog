from flask import Flask
from flask_bootstrap import Bootstrap5


app = Flask(__name__)
bootstrap = Bootstrap5(app)


@app.route('/')
def index():
    return 'Hi everyone'

if __name__=='__main__':
    app.run()