import os
from dotenv import load_dotenv
from flask_migrate import Migrate

dotenv_path = os.path.join(os.path.dirname(__file__),'.env')
print(dotenv_path)
if os.path.exists(dotenv_path):
    denv = load_dotenv(dotenv_path)



from app import create_app, db
from app.models import User,Role

app = create_app(os.getenv('FLASK_CONFIG') or 'default')
migrate = Migrate(app,db)

@app.shell_context_processor
def make_shell_context():
    return dict(app=app, db=db,User=User,Role=Role)

@app.cli.command()
def test():
    """ Run the unit tests."""
    import unittest
    tests = unittest.TestLoader().discover('tests')
    unittest.TextTestRunner(verbosity=2).run(tests)

