from datetime import datetime

from flask import render_template, session, redirect, url_for,flash

from . import main
from .forms import NameForm
from .. import db
from ..models import User

@main.route('/',methods=['GET','POST'])
def index():
    # name = ''
    form = NameForm()
    if form.validate_on_submit():
        old_name = session.get('name')
        if old_name is not None and old_name != form.name.data:
            flash('Looks like you have changed your name!')
        session['name'] = form.name.data
        print(session)
        redirect(url_for('main.index'))
        # name = form.name.data
        # form.name.data = ''
        # user = User.query.filter_by(username=form.name.data).first()
    # return render_template('form.html',form=form, name=name)
    return render_template('form.html',form=form, name=session.get('name'))
